#include <stdio.h>
#include <omp.h>

void print_2d_array(int **array, int rows, int cols) {
    int i, j, k;
    for(i = 0; i < rows; i++) {
        printf("\n\tRow %d: ", i + 1);
        for(j = 0; j < cols; j++) {
            printf("%d ", array[i][j]);
        }
    }
    printf("\n");
}

int main() {
    int i, j, k, l;
    int number;

    srand(time(NULL));
    double start, end;

    FILE* fptr;
    char filename[12] = "zad4-x.txt";

    int n[3] = {100, 250, 500};
    int m[3] = {250, 500, 1000};
    int p[3] = {500, 1000, 2000};

    for(l = 0; l < 3; l++) {
        //Alokacja pamięci dla tablic
        int **arr = (int **)malloc(n[l] * sizeof(int *));
        for(i = 0; i < n[l]; i++) {
            arr[i] = (int *)malloc(m[l] * sizeof(int));
        }

        int **arr2 = (int **)malloc(m[l] * sizeof(int *));
        for(i = 0; i < m[l]; i++) {
            arr2[i] = (int *)malloc(p[l] * sizeof(int));
        }

        int **res_arr = (int **)malloc(n[l] * sizeof(int *));
        for(i = 0; i < n[l]; i++) {
            res_arr[i] = (int *)malloc(p[l] * sizeof(int));
        }

        //Wypełnianie tablic danymi
        for(i = 0; i < n[l]; i++) {
            for(j = 0; j < m[l]; j++) {
                arr[i][j] = rand()%100;
            }
        }

        for(i = 0; i < m[l]; i++) {
            for(j = 0; j < p[l]; j++) {
                arr2[i][j] = rand()%100;
            }
        }

        for(i = 0; i < n[l]; i++) {
            for(j = 0; j < p[l]; j++) {
                res_arr[i][j] = 0;
            }
        }

        //Wypisanie tablic
        // printf("\nMacierz nr 1:");
        // print_2d_array(arr, n[l], m[l]);
        
        // printf("\nMacierz nr 2:");
        // print_2d_array(arr2, m[l], p[l]);

        //Obliczenie iloczynu macierzy
        start = omp_get_wtime();
        #pragma omp parallel private(i, j, k) shared(res_arr) num_threads(4)
        {
            #pragma omp for schedule(dynamic)  //Pętla zewnętrzna
            for(i = 0; i < n[l]; i++) {

                for(j = 0; j < p[l]; j++) {
                    //#pragma omp for schedule(dynamic)  //Pętla wewnętrzna
                    for(k = 0; k < m[l]; k++) {
                        //#pragma omp atomic update
                        res_arr[i][j] += arr[i][k] * arr2[k][j];
                    }
                }
            }
        }
    
        end = omp_get_wtime();


        printf("Wymiary macierzy nr 1: %d x %d\n", n[l], m[l]);
        printf("Wymiary macierzy nr 2: %d x %d\n", m[l], p[l]);
        printf("Wymiary macierzy wynikowej: %d x %d\n", n[l], p[l]);
        printf("Czas obliczania iloczynu macierzy: %f\n\n", end - start);

        filename[5] = l + '0';
        fptr = fopen(filename, "a");
        if(!fptr) {
            printf("Błąd otwarcia pliku!");
        } else {
            fprintf(fptr, "%lf\n", end - start);
        }
        

        //printf("\nMacierz wynikowa:");
        //print_2d_array(res_arr, n[l], p[l]);
        //printf("-----------------------------------------------------------------\n");

        //Zwalnianie pamięci
        for(i = 0; i < n[l]; i++) {
            free(arr[i]);
        }
        free(arr);

        for(i = 0; i < m[l]; i++) {
            free(arr2[i]);
        }
        free(arr2);

        for(i = 0; i < n[l]; i++) {
            free(res_arr[i]);
        }
        free(res_arr);
    }
    

	return 0;
}
