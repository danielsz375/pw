#include <stdio.h>

void print_2d_array(int **array, int rows, int cols) {
    int i, j, k;
    for(i = 0; i < rows; i++) {
        printf("\nRow %d: ", i + 1);
        for(j = 0; j < cols; j++) {
            printf("%d ", array[i][j]);
        }
    }
    printf("\n");
}

int main() {
    int i, j, k;
    int number;
    srand(time(NULL));

    int dim[3] = {2, 3, 4};

    //Alokacja pamięci dla tablic
    int **arr = (int **)malloc(dim[0] * sizeof(int *));
    for(i = 0; i < dim[0]; i++) {
        arr[i] = (int *)malloc(dim[1] * sizeof(int));
    }

    int **arr2 = (int **)malloc(dim[1] * sizeof(int *));
    for(i = 0; i < dim[1]; i++) {
        arr2[i] = (int *)malloc(dim[2] * sizeof(int));
    }

    int **res_arr = (int **)malloc(dim[0] * sizeof(int *));
    for(i = 0; i < dim[0]; i++) {
        res_arr[i] = (int *)malloc(dim[2] * sizeof(int));
    }

    //Wypełnianie tablic danymi
    for(i = 0; i < dim[0]; i++) {
        for(j = 0; j < dim[1]; j++) {
            arr[i][j] = rand()%100;
        }
    }

    for(i = 0; i < dim[1]; i++) {
        for(j = 0; j < dim[2]; j++) {
            arr2[i][j] = rand()%100;
        }
    }

    for(i = 0; i < dim[0]; i++) {
        for(j = 0; j < dim[2]; j++) {
            res_arr[i][j] = 0;
        }
    }

    //Wypisanie tablic
    print_2d_array(arr, dim[0], dim[1]);
    print_2d_array(arr2, dim[1], dim[2]);

    for(i = 0; i < dim[0]; i++) {
        for(j = 0; j < dim[2]; j++) {
            for(k = 0; k < dim[1]; k++) {
                res_arr[i][j] += arr[i][k] * arr2[k][j];
            }
        }
    }

    printf("\nMacierz wynikowa:");
    print_2d_array(res_arr, dim[0], dim[2]);

    //Zwalnianie pamięci
    for(i = 0; i < dim[0]; i++) {
        free(arr[i]);
    }
    free(arr);

    for(i = 0; i < dim[1]; i++) {
        free(arr2[i]);
    }
    free(arr2);

    for(i = 0; i < dim[0]; i++) {
        free(res_arr[i]);
    }
    free(res_arr);

	return 0;
}
