package com.company;

import java.util.Random;

class Thr extends Thread {

    private int threadId; // numer wątku
    private int threadsNumber; // liczba wszystkich wątków
    private int vector[]; // referencja do wektora nr 1
    private int vector2[]; // referencja do wektora nr 2
    private int resultVector[]; // referencja do wektora wynikowego


    public Thr(int threadId,
               int threadsNumber,
               int vector[],
               int vector2[],
               int resultVector[]) {
        this.threadId = threadId;
        this.threadsNumber = threadsNumber;
        this.vector = vector;
        this.vector2 = vector2;
        this.resultVector = resultVector;
    }

    @Override
    public void run() {
        for(int i = threadId; i < resultVector.length; i += threadsNumber) {
            resultVector[i] = vector[i] + vector2[i];
        }
    }
}

public class Losthr {

    public static void main(String args[]) {

        Random random = new Random();
        int i;
        int threadsNumber = 8;
        int vectorLength = 20;

        System.out.println("Długość pojedynczego wektora: " + vectorLength);
        System.out.println("Liczba wątków: " + threadsNumber);


        int vector[] = new int[vectorLength];
        int vector2[] = new int[vectorLength];
        int resultVector[] = new int[vectorLength];

        Thr[] NewThr = new Thr[threadsNumber];


        for (i = 0; i < vectorLength; i++) {
            vector[i] = random.nextInt(10);
            vector2[i] = random.nextInt(10);
        }


        for (i = 0; i < threadsNumber; i++) {
            (NewThr[i] = new Thr(i, threadsNumber, vector, vector2, resultVector)).start();
        }


        for (i = 0; i < threadsNumber; i++) {
            try {
                NewThr[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        System.out.println("Wektor nr 1:");
        for (i = 0; i < vectorLength; i++) {
            System.out.print(vector[i] + " ");
        }


        System.out.println("\nWektor nr 2:");
        for (i = 0; i < vectorLength; i++) {
            System.out.print(vector2[i] + " ");
        }


        System.out.println("\nWektor wynikowy:");
        for (i = 0; i < vectorLength; i++) {
            System.out.print(resultVector[i] + " ");
        }
    }
}