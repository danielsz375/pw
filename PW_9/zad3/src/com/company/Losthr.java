package com.company;

class Thr extends Thread {

    private int threadId; //numer wątku
    private boolean primeNumbers[]; // referencja do tablicy
    private int startNumber; // początek przedziału
    private int endNumber; // koniec przedziału

    public Thr(int threadId, int startNumber, int amount, boolean[] primeNumbers) {
        this.threadId = threadId;
        this.primeNumbers = primeNumbers;
        this.startNumber = startNumber;
        this.endNumber = startNumber + amount;
    }

    @Override
    public void run() {
        System.out.println("Wątek " + threadId + " wyklucza wielokrotności liczb z przedziału: " + startNumber + " - " + (endNumber - 1));
        for (int i = startNumber; i < endNumber; i++) {
            if(primeNumbers[i]) {
                for (int k = 2; i * k < primeNumbers.length; k++) {
                    primeNumbers[i * k] = false;
                }
            }
        }
    }
}

public class Losthr {

    public static void main(String args[]) {

        int threadsNumber = 7;
        int n = 198; // ilość liczb do podziału między wątki
        int numbersPerThread = n / threadsNumber; // ilość liczb przypadających na jeden wątek
        int unassignedNumbers = n - (numbersPerThread * threadsNumber); // ilość liczb nieprzypisanych do wątków

        boolean primeNumbers[] = new boolean[n + 2];

        for (int i = 0; i < primeNumbers.length; i++) {
            primeNumbers[i] = true;
        }
        primeNumbers[0] = false;
        primeNumbers[1] = false;

        Thr[] NewThr = new Thr[threadsNumber];

        int startNumber = 2;
        int amount;
        for (int i = 0; i < threadsNumber; i++) {
            if(unassignedNumbers > 0) {
                unassignedNumbers--;
                amount = numbersPerThread + 1;
            } else {
                amount = numbersPerThread;
            }
            (NewThr[i] = new Thr(i, startNumber, amount, primeNumbers)).start();
            startNumber += amount;
        }

        for (int i = 0; i < threadsNumber; i++) {
            try {
                NewThr[i].join();
            } catch (InterruptedException e) {}
        }

        System.out.println("Znalezione liczby pierwsze: ");
        for (int i = 1; i < primeNumbers.length; i++) {
            if(primeNumbers[i]) {
                System.out.print(i + " ");
            }
            if(i % 100 == 0) {
                System.out.println(" ");
            }
        }

    }
}