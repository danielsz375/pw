package com.company;

import java.util.Random;

class Thr extends Thread {

    private int threadId; //numer wątku
    private int threadsNumber; //liczba wszystkich wątków
    private char charArray[]; //tablica możliwych znaków
    private int counter[]; //tablica zawierająca liczbę wystąpień znaków o tym samym indeksie w tablicy charArray
    private char image[][]; //obraz

    public Thr(int threadId, int threadsNumber, char[] charArray, int[] counter, char[][] image) {
        this.threadId = threadId;
        this.threadsNumber = threadsNumber;
        this.charArray = charArray;
        this.counter = counter;
        this.image = image;
    }

    @Override
    public void run() {
        //Pętla iterująca przez wiersze obrazu
        for(int i = 0; i < image.length; i++) {
            //Pętla iterująca przez kolumny obrazu
            for(int j = 0; j < image[i].length; j++)
                //Pętla iterująca przez znaki przypisane danemu wątkowi
                for(int k = threadId; k < charArray.length; k += threadsNumber) {
                    if (image[i][j] == charArray[k]) {
                        counter[k]++;
                        break;
                    }
                }
        }

        for(int k = threadId; k < charArray.length; k += threadsNumber) {
            System.out.println("Wątek " + threadId  + ": " + charArray[k] + " " + counter[k] + "x");
        }

//        for(int i = threadId; i < charArray.length; i += threadsNumber) {
//            counter = 0;
//            for(int j = 0; j < image.length; j++) {
//                for(int k = 0; k < image[j].length; k++)
//                if (image[j][k] == charArray[i]) {
//                    counter++;
//                }
//            }
//            System.out.println("Wątek " + threadId + ": " + charArray[i] + " " + counter + "x");
//        }
    }
}

public class Losthr {

    public static void main(String args[]) {

        Random random = new Random();
        int threadsNumber = 8;

        //char charArray[] = {'!', '@', '#', '$', '%', '^', '&', '*', '(', ')'};

        int firstChar = 33;
        int charAmount = 94;

        char charArray[] = new char[charAmount];
        int counter[] = new int[charAmount];

        for(int i = 0; i < charAmount; i++) {
            charArray[i] = (char)(firstChar + i);
            counter[i] = 0;
        }

        //int n = 3, m = 4;
        int n = 10, m = 30;

        char image[][] = new char[n][];
        for(int i = 0; i < n; i++) {
            image[i] = new char[m];
        }

        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                image[i][j] = charArray[random.nextInt(charArray.length)];
            }
        }

        System.out.println("Zawartość obrazu:");
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                System.out.print(image[i][j] + " ");
            }
            System.out.println(" ");
        }

        Thr[] NewThr = new Thr[threadsNumber];

        for (int i = 0; i < threadsNumber; i++) {
            (NewThr[i] = new Thr(i, threadsNumber, charArray, counter, image)).start();
        }

        for (int i = 0; i < threadsNumber; i++) {
            try {
                NewThr[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}