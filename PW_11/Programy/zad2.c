#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

int main(int argc, char *argv[])
{
	int myrank;
	int npes;

	double start, end;
	double localsum = 0;
	double globalsum = 0;

	int root = 0;
	int k, totalNOE;
	int divider, unassignedNumbers;
	int startNumber = 0, NOE; // NOE - Number Of Elements

	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &npes);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	if (myrank == 0) {
		printf("Podaj liczbę elementów sumy:\n");
		scanf("%d", &totalNOE);
	}	

	start = MPI_Wtime();
  
	MPI_Bcast(&totalNOE, 1, MPI_INT, root, MPI_COMM_WORLD );

	divider = totalNOE / npes;
	unassignedNumbers = totalNOE % npes;

	//printf("Divider: %d, unassignedNumbers %d\n", divider, unassignedNumbers);
	if (myrank >= unassignedNumbers) {
		startNumber += (divider + 1) * unassignedNumbers;
		startNumber += (myrank - unassignedNumbers) * divider;
		NOE = divider;
		//printf("if %d %d\n", startNumber, NOE);
	} else {
		startNumber += (divider + 1) * myrank;
		NOE = divider + 1;
		//printf("else %d %d\n", startNumber, NOE);
	}

	for (k = startNumber; k < startNumber + NOE; k++ ) {
		if (k == 0) 
			continue;
		localsum += 1.0 / k;
	}
	printf("rank = %d, start = %d, last = %d (number of elements: %d)\n", myrank, startNumber, startNumber + NOE - 1, NOE);
	//printf("Localsum %lf\n", localsum);

	MPI_Reduce( &localsum, &globalsum, 1, MPI_DOUBLE, MPI_SUM, root, MPI_COMM_WORLD );

	end = MPI_Wtime();

	if(myrank == 0){
		printf("Obliczone g(%d): %lf\n", totalNOE, globalsum - log(totalNOE));
		printf("Czas działania programu: %lf\n", end-start);
	}
	
	MPI_Finalize();
	return 0;
}
