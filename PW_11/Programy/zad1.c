#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

int main(int argc, char *argv[])
{

	int npes;
	int myrank;
	double start, end;

	int root = 0;
	int i,j,k,S[10],n,p;

    n = atoi(argv[1]);

	MPI_Init(&argc, &argv);
	
	start = MPI_Wtime();
	MPI_Comm_size(MPI_COMM_WORLD, &npes);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	MPI_Bcast(&n, 1, MPI_INT, root, MPI_COMM_WORLD );

	double localsum = 0;
	double globalsum = 0;

	for (i = myrank; i < n; i += npes ) {
	    localsum += pow(-1, i) / (2 * i + 1);
	}
    localsum *= 4;

	MPI_Reduce( &localsum, &globalsum, 1, MPI_DOUBLE, MPI_SUM, root, MPI_COMM_WORLD );

	end = MPI_Wtime();

	if(!myrank){
		printf("Obliczona wartość liczby PI: %lf\n", globalsum);
		printf("Czas działania programu: %lf\n", end-start);
	}
	
	MPI_Finalize();
	return 0;
}