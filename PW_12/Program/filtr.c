#include <mpi.h>
#include "pbm.h"


int main(int argc, char *argv[]) {
    image in;
    image out;
    
    //Pionowy filtr Sobel'a, wykrywający krawędzie pionowe
    int filter[] = {1, 0, -1,
                    2, 0, -2,
                    1, 0, -1};

    int npes;
	int myrank; 
    int root = 0;
    int image_tag = 15;
    int image_info[3];

    int width;
    int height;
    int maxValue; 

    int rows_per_proc;
    int row_startpoint;
    
    int i, j, k;
    int sum;

    uchar **image_part;
    uchar **new_image_part;

    double start, start_write, end, end_read, time, time_read, time_write;

    
  
	MPI_Init(&argc, &argv);
    MPI_Status status;
	
	MPI_Comm_size(MPI_COMM_WORLD, &npes);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);


    //1. Wczytanie danych przez proces 0
    if(myrank == root) {
        start = MPI_Wtime();
        readInput(argv[1], &in);
        end_read = MPI_Wtime();
        time_read = end_read - start;
        
        width = in.width;
        height = in.height;
        maxValue = in.maxValue; 

        out.height = in.height;
        out.width = in.width;
        out.maxValue = in.maxValue;
        out.pixel = (uchar*)malloc(sizeof(uchar) * out.height * out.width);

        memcpy(out.type, in.type, TYPE_LEN+1);

        //2.a Wysłanie danych o obrazie do pozostałych procesów
        image_info[0] = width;
        image_info[1] = height;
        image_info[2] = maxValue;

        for(i = 1; i < npes; i++) {
            MPI_Send(&image_info, 3, MPI_INT, i, 13, MPI_COMM_WORLD);
        }

    }


	if (myrank != root) {
        //2.b Odebranie danych o obrazie przez procesy
	    MPI_Recv(&image_info, 3, MPI_INT, root, 13, MPI_COMM_WORLD, &status);
        width = image_info[0];
        height = image_info[1];  
        maxValue = image_info[2];
	}

    rows_per_proc = height / npes;
    row_startpoint = myrank * rows_per_proc;
    if(myrank == (npes - 1) &&  height % npes > 0){
        rows_per_proc = height - rows_per_proc * (npes - 1 );
    }



    //3.a Alokacja pamięci dla części obrazu modyfikowanej przez dany proces
    image_part = (uchar **)malloc(sizeof(uchar *) * rows_per_proc);
    new_image_part = (uchar **)malloc(sizeof(uchar *) * rows_per_proc);

    //3.b Alokacja pamięci dla skrajnych wierszy
    uchar * first_row = (uchar*)malloc(sizeof(uchar) * width);
    uchar * last_row = (uchar*)malloc(sizeof(uchar) * width);

    uchar * received_up = (uchar*)malloc(sizeof(uchar) * width);
    uchar * received_down = (uchar*)malloc(sizeof(uchar) * width);

    if (image_part == NULL || new_image_part == NULL  
        || first_row == NULL || last_row == NULL
        || received_up == NULL || received_down == NULL) {
            printf("1. Allocation error");
            exit(-1);
    }

    for(i = 0; i < rows_per_proc; i++) {
        image_part[i] = (uchar*)malloc((width + 2) * sizeof(uchar));
        new_image_part[i] = (uchar*)malloc((width + 2) * sizeof(uchar));

        if (image_part[i] == NULL || new_image_part[i] == NULL) {
            printf("2. Allocation error");
            exit(-2);
        }

        for(j = 0; j < width; j++) {
            image_part[i][j] = new_image_part[i][j] = 0;
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if(myrank == root) {
        //4. Wysłanie fragmentów obrazu do pozostałych procesów przez proces 0
        int real_rows_number = height / npes;
        int base_rows_number = real_rows_number;
        for(i = 1; i < npes; i++) {
            if(i != root) {
                if(i == npes - 1) {
                    real_rows_number = height - real_rows_number*(npes - 1);
                }    
                MPI_Send(in.pixel + base_rows_number*width*i, real_rows_number*width, MPI_UNSIGNED_CHAR, i, 14, MPI_COMM_WORLD);
            }
        }

        //5.a Proces 0 przepisuje swoją część obrazu z bufora do image_part
        uchar* buff = in.pixel + root * base_rows_number*width;

        for(i = 0; i < rows_per_proc; i++) {
            for(j = 0; j < width; j++) {
                image_part[i][j+1] = buff[i*width + j];
            }
        }
    } else {
        //5.a Procesy odbierają i przepisują swoją część obrazu z bufora do image_part
        uchar* recv_buff=(uchar*)malloc(sizeof(uchar) * width*rows_per_proc);

        MPI_Recv(recv_buff, rows_per_proc*width, MPI_UNSIGNED_CHAR, root, 14, MPI_COMM_WORLD, &status);

        for(i = 0; i < rows_per_proc; i++) {
            for(j = 0; j < width; j++) {
                image_part[i][j + 1]=  recv_buff[i*width + j];
            }
        }
        //printf("Received");
    }

    //Przesyłanie skrajnych wierszy
    if(myrank != 0) {
        memcpy(first_row, image_part[0], width); 
        MPI_Send(&first_row[0], width, MPI_UNSIGNED_CHAR, myrank-1, myrank-1, MPI_COMM_WORLD);
    }
    if(myrank != npes - 1) {
        memcpy(last_row, image_part[height / npes - 1], width); 
        MPI_Send(&last_row[0], width, MPI_UNSIGNED_CHAR, myrank+1, myrank+1, MPI_COMM_WORLD);
    }

    //Odbieranie skrajnych wierszy
    if(myrank != 0) {
        MPI_Recv(&received_up[0], width, MPI_UNSIGNED_CHAR, myrank-1, myrank, MPI_COMM_WORLD, &status);
    }
    if(myrank != npes - 1) {
        MPI_Recv(&received_down[0], width, MPI_UNSIGNED_CHAR, myrank+1, myrank, MPI_COMM_WORLD, &status);
    }

    

    //6. Stosowanie filtra
    for(i = 0; i < rows_per_proc; i++) {
	    for(j = 0; j <= width; j++) {
	        sum = 0;

            //1. wiersz
            if(i > 0) {
                for(k = -1; k <= 1; k++) {
                    sum += filter[k + 1] * image_part[i - 1][j + k];
                }
            } else {
                if(myrank > 0) {
                    for(k = -1; k <= 1; k++) {
                        sum += filter[k + 1] * received_up[j + k];
                    }
                } else {
                    //Duplikacja pierwszego wiersza dla procesu 0
                    // for(k = -1; k <= 1; k++) {
                    //     sum += filter[k + 1] * image_part[i][j + k];
                    // }
                }
            }
		        
            //2. wiersz
	        for(k = -1; k <= 1; k++) {
                sum += filter[k + 4] * image_part[i][j + k];
            }
		        
            //3. wiersz
            if(i < rows_per_proc - 1) {
                for(k = -1; k <= 1; k++) {
		            sum += filter[k + 7] * image_part[i + 1][j + k];
                }
            } else {
                if(myrank < npes - 1) {
                    for(k = -1; k <= 1; k++) {
                        sum += filter[k + 7] * received_down[j + k];
                    }
                } else {
                    //Duplikacja ostatniego wiersza dla ostatniego procesu
                    // for(k = -1; k <= 1; k++) {
                    //     sum += filter[k + 7] * image_part[i][j + k];
                    // }
                }
            }

	        if(sum > maxValue) {
                sum = maxValue;
            } else if (sum < 0) {
                sum = 0;
            }
		    
	        new_image_part[i][j] = sum;
	    }
    }

    

    if(myrank != root) {
        //7.a Procesy przepisują image_part do bufora i odsyłajądo procesu 0
        uchar* send_buff = (uchar*)malloc(width * rows_per_proc * sizeof(uchar));
        for(i = 0; i < rows_per_proc; i++)
            for(j = 1; j <= width; j++) {
                send_buff[width*i + (j - 1)] = new_image_part[i][j];
            }

        MPI_Send(send_buff, rows_per_proc*width, MPI_UNSIGNED_CHAR, root, image_tag, MPI_COMM_WORLD);
        free(send_buff);
    } else {
        //7.b Proces 0 odbiera dane 
        int real_rows_number = height / npes;
        int base_rows_number = real_rows_number;
        
        for(i = 0; i < npes; i++) {
            if(i != root) {
                if(i == npes - 1) {
                    real_rows_number = height - (npes - 1) * real_rows_number;
                }
                MPI_Recv (out.pixel + base_rows_number*width*i, real_rows_number*width, MPI_UNSIGNED_CHAR, i, image_tag, MPI_COMM_WORLD, &status);                
            }
        }

        //7.c Proces 0 przepisuje odebrane dane do obrazu wynikowego out
        for(i = 0; i < rows_per_proc; i++) {
            for(j = 1; j <= width; j++) {
                out.pixel[myrank*width + width*i + (j - 1)] = new_image_part[i][j];
            }
        }

    }

    for(i = 0; i < rows_per_proc; i++){
        free(image_part[i]);
        free(new_image_part[i]);
    }
    free(image_part);
    free(new_image_part);


    if (myrank == 0) {
        start_write = MPI_Wtime();
        writeData(argv[2], &out);
        end = MPI_Wtime();
        time_read = end - start_write;
        time = end - start;
    }
	MPI_Finalize();

	if (myrank == 0) {
        time += time_read;
        printf("Liczba procesów: %d\n", npes);
        printf("Czas odczytu i zapisu: %lf\n", time_read + time_write);
        printf("Czas wykonania programu: %lf\n", time);
        char filename[50] = "times_read_write.txt";
        FILE *fp = fopen(filename, "a");
        fprintf(fp, "%d: %lf\n", npes, time_read + time_write);

        char filename2[50] = "times_execution.txt";
        FILE *fp2 = fopen(filename2, "a");
        fprintf(fp2, "%d: %lf\n", npes, time);

        fclose(fp);
        fclose(fp2);
	}

	return 0;
}
