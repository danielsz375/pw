#include<stdlib.h> 
#include<stdio.h> 
#include<time.h>
#include<pthread.h>

int ilosc_liczb = 400;
int *czy_pierwsza;

void *sito_Erastotenesa(void *arg)
{
    int i2, i = (int)arg;
    for (i2 = i*i; i2 < ilosc_liczb; i2 += i) {
        czy_pierwsza[i2] = 0;
    }
}

int main()
{
    czy_pierwsza = malloc(sizeof(int) * ilosc_liczb);
    pthread_t threads[20];
    int nr_watku = 0;
    int liczba;

    //Wypełnianie tablicy wartościami
    //Indeks tablicy reprezentuje liczbę
    //Dla indeksów reprezentujących liczby pierwsze tablica zawiera wartość 1
    //Wstępnie zakładamy, że wszystkie liczby, poza 0 i 1, są pierwsze, a następnie za pomocą sita Erastotenesa odsiewamy liczby złożone
    czy_pierwsza[0] = 0;
    czy_pierwsza[1] = 0;
    for (liczba = 2; liczba < ilosc_liczb; liczba++) {
        czy_pierwsza[liczba] = 1;
    }

    //Eliminacja wielokrotności przy wykorzystaniu wątków
    for (liczba = 2; liczba*liczba < ilosc_liczb; liczba++) {
        if(czy_pierwsza[liczba]) {
            pthread_create(&threads[nr_watku], NULL, sito_Erastotenesa, liczba);         
            nr_watku++;
        }
    }

    int i;
    for (i = 0; i < nr_watku; i++) {
        pthread_join(threads[i], NULL);
    }

    //Wypisanie znalezionych liczb
    printf("Znalezione liczby pierwsze:\n");
    for(liczba = 0; liczba < ilosc_liczb; liczba++) {
        if(czy_pierwsza[liczba]) printf("%d ", liczba);
    }
    printf("\n");
        
    free(czy_pierwsza);
}


