#include<stdlib.h> 
#include<stdio.h> 
#include<time.h>
#include<pthread.h>
#define WATKI 2

struct tsk {
    int *array;
    int left;
    int right;
};
struct tsk tsk[WATKI + 1];
pthread_mutex_t mutex[2];

void mergeSort(int* arr, int l, int r);
void merge(int arr[], int l, int m, int r);

void *thread_sort(void* arg) {
    struct tsk *tsk = arg;
    mergeSort(tsk->array, tsk->left, tsk->right);
    return 0;
}

void* thread_merge_arrays(void* arg) {
    int i;
    for (i = 0; i < WATKI; i++) {
        pthread_mutex_lock(&mutex[i]);
    }
    
    int j = 0, k = 0;
    i = 0;
    while ((i < tsk[0].right + 1) && (j < tsk[1].right + 1)) {
        if(tsk[0].array[i] < tsk[1].array[j]) {
            tsk[2].array[k] = tsk[0].array[i];
            ++i;
            ++k;
        }
        else {
            tsk[2].array[k] = tsk[1].array[j];
            ++j;
            ++k;
        }
    }

    while (i < tsk[0].right + 1) tsk[2].array[k++] = tsk[0].array[i++];
    while (j < tsk[1].right + 1) tsk[2].array[k++] = tsk[1].array[j++];

    tsk[2].left = 0;
    tsk[2].right = tsk[0].right + tsk[1].right + 1 ;

    for (i = 0; i < WATKI; i++) {
        pthread_mutex_unlock(&mutex[i]);
    }
}

int main() {
    int threads_number = WATKI;
    pthread_t threads[threads_number + 1];
    int i, k;
    int arr_size = 15;

    //Inicjalizacja początkowego ciągu
    srand(time(NULL));
    int* array = malloc(sizeof(int) * arr_size);
    for(i = 0; i < arr_size; i++) {
        array[i] = rand() % 100; 
    }

    //Wypisanie początkowego ciągu
    printf("Początkowy ciąg:\n");
    for (i = 0; i < arr_size; i++) {
        printf("%d ", array[i]);
    }

    //Podział ciągu na 2 podciągi
    int arr1_size = arr_size / 2;
    int arr2_size = arr_size - arr1_size;

    tsk[0].array = malloc(sizeof(int) * arr1_size);
    tsk[1].array = malloc(sizeof(int) * arr2_size);
    tsk[2].array = malloc(sizeof(int) * arr_size);

    tsk[0].left = 0;
    tsk[0].right = arr1_size - 1;
    for(k = 0; k < arr1_size; k++) {
        tsk[0].array[k] = array[k]; 
    }
    
    tsk[1].left = 0;
    tsk[1].right = arr2_size - 1;
    for(i = 0; i < arr2_size; i++, k++) {
        tsk[1].array[i] = array[k]; 
    }

    tsk[2].left = 0;
    tsk[2].right = arr_size - 1;

    //Wypisanie podciągów
    printf("\n\nPodciąg nr 1 przed sortowaniem:\n");
    for (i = 0; i < arr1_size; i++) {
        printf("%d ", tsk[0].array[i]);
    }

    printf("\n\nPodciąg nr 2 przed sortowaniem:\n");
    for (i = 0; i < arr2_size; i++) {
        printf("%d ", tsk[1].array[i]);
    }

    //Inicjalizacja muteksów
    for(i = 0; i < 2; i++) {
        pthread_mutex_init(&mutex[i], NULL);
    }

    //Uruchomienie wątków sortujących
    for (i = 0; i < threads_number; i++) {
        pthread_mutex_lock(&mutex[i]);
        pthread_create(&threads[i], NULL, thread_sort, &tsk[i]);
    }

    //Uruchomienie wątku scalającego
    pthread_create(&threads[2], NULL, thread_merge_arrays, NULL);

    //Join dla wątków sortujących
    for (i = 0; i < threads_number; i++) {
         pthread_join(threads[i], NULL);
         pthread_mutex_unlock(&mutex[i]);
    }
    //Join dla wątku scalającego
    pthread_join(threads[2], NULL);
       
    printf("\n\n\nPodciąg nr 1 po sortowaniu:\n");
    for (i = 0; i < arr1_size; i++) {
        printf("%d ", tsk[0].array[i]);
    }

    printf("\n\nPodciąg nr 2 po sortowaniu:\n");
    for (i = 0; i < arr2_size; i++) {
        printf("%d ", tsk[1].array[i]);
    }

    printf("\n\nZłączony ciąg:\n");
    for (i = 0; i < arr_size; i++) {
        printf("%d ", tsk[2].array[i]);
    }
    printf("\n");

    //Zwalnianie pamieci
    for(i = 0; i < 3; i++) {
        free(tsk[i].array);
    }
    free(array);
}

void mergeSort(int* arr, int l, int r) { 
    int m;
    if (l < r) { 
        m = l + (r - l)/2; 
        mergeSort(arr, l, m); 
        mergeSort(arr, m + 1, r); 
        merge(arr, l, m, r); 
    } 
} 

void merge(int arr[], int l, int m, int r) { 
    int i, j, k, n1, n2; 
    n1 = m - l + 1; 
    n2 = r - m; 
    int L[n1], R[n2]; 
  
    for (i = 0; i < n1; i++) L[i] = arr[l + i]; 
    for (j = 0; j < n2; j++) R[j] = arr[m + 1 + j]; 
  
    i = 0; 
    j = 0; 
    k = l; 
    while (i < n1 && j < n2) { 
        if (L[i] <= R[j]) { 
            arr[k] = L[i]; 
            i++; 
        } 
        else { 
            arr[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 
  
    while (i < n1) { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    } 
  
    while (j < n2) { 
        arr[k] = R[j]; 
        k++;
        j++; 
    } 
} 

