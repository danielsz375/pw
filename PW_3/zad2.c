#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/syscall.h>


void *print_thread_id(void* ptr_number) {
    int number = (int)ptr_number;
    printf("Watek nr %d, gettid = %d, pthread_self = %d\n", number + 1, syscall(SYS_gettid), pthread_self());
}

int main() {
    pthread_t my_threads[10];
    int threads_number = 10;

    for(int i = 0; i < threads_number; i++) {
        pthread_create(&my_threads[i], NULL, print_thread_id, (void*) i);
    }

    for(int i = 0; i < threads_number; i++) {
        pthread_join(my_threads[i], NULL);
        printf("Koniec watku nr %d\n", i + 1);
    }

    return 0;
}