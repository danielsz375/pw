#define _GNU_SOURCE  
#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<limits.h>
#include<errno.h>
#include<sched.h>
#include<sys/syscall.h>


#define WYMIAR 1000
#define ROZMIAR WYMIAR*WYMIAR
#define WATKI 10
#define RETVAL 5

int zmienna_wspolna=0;
double a[ROZMIAR],b[ROZMIAR],c[ROZMIAR];


double czasozajmowacz(){
  int i, j, k;
  int n=WYMIAR;
  for(i=0;i<ROZMIAR;i++) a[i]=1.0*i;
  for(i=0;i<ROZMIAR;i++) b[i]=1.0*(ROZMIAR-i);
  for(i=0;i<n;i++){
    for(j=0;j<n;j++){
      c[i+n*j]=0.0;
      for(k=0;k<n;k++){
	c[i+n*j] += a[i+n*k]*b[k+n*j];
      }
    }
  }
  return(c[ROZMIAR-1]);
}

#define test_errno(msg) do{if (errno) {printf(msg); exit(EXIT_FAILURE);}} while(0)
#define handle_error_en(en, msg) \
               do { errno = en; printf(msg); exit(EXIT_FAILURE); } while (0)
#define errExitEN(en, msg) \
                        do { errno = en; printf(msg); \
                             exit(EXIT_FAILURE); } while (0)


void display_pthread_attr(pthread_attr_t *attr, void *arg_wsk)
{
    int s;
    size_t stacksize;
    size_t guardsize;
    int policy;
    struct sched_param schedparam;
    int detachstate;
    int inheritsched;
	int stackaddr;

    printf("Atrybuty wątku nr %d: \n", arg_wsk);
    
	errno = pthread_attr_getstack(attr, &stackaddr, &stacksize);
	test_errno("pthread_attr_getstackaddr");
	printf("Stack addr:          %x\n", stackaddr);

    //s = pthread_attr_getstacksize(attr, &stacksize);
    // if (s != 0)
    //     errExitEN(s, "pthread_attr_getstacksize");
    printf("Stack size:          %zd\n", stacksize);

    s = pthread_attr_getguardsize(attr, &guardsize);
    if (s != 0)
        errExitEN(s, "pthread_attr_getguardsize");
    printf("Guard size:          %zd\n", guardsize);

    s = pthread_attr_getschedpolicy(attr, &policy);
    if (s != 0)
        errExitEN(s, "pthread_attr_getschedpolicy");
    printf("Scheduling policy:   %s\n",
            (policy == SCHED_FIFO) ? "SCHED_FIFO" :
            (policy == SCHED_RR) ? "SCHED_RR" :
            (policy == SCHED_OTHER) ? "SCHED_OTHER" : "[unknown]");

    s = pthread_attr_getschedparam(attr, &schedparam);
    if (s != 0)
        errExitEN(s, "pthread_attr_getschedparam");
    printf("Scheduling priority: %d\n", schedparam.sched_priority);

    s = pthread_attr_getdetachstate(attr, &detachstate);
    if (s != 0)
        errExitEN(s, "pthread_attr_getdetachstate");
    printf("Detach state:        %s\n",
            (detachstate == PTHREAD_CREATE_DETACHED) ? "DETACHED" :
            (detachstate == PTHREAD_CREATE_JOINABLE) ? "JOINABLE" :
            "???");

    s = pthread_attr_getinheritsched(attr, &inheritsched);
    if (s != 0)
        errExitEN(s, "pthread_attr_getinheritsched");
    printf("Inherit scheduler:   %s\n",
            (inheritsched == PTHREAD_INHERIT_SCHED) ? "INHERIT" :
            (inheritsched == PTHREAD_EXPLICIT_SCHED) ? "EXPLICIT" :
            "???");
}


void *zadanie_watku(void *arg_wsk) {
    int s, rc = 0;
    pthread_attr_t attr;
    pthread_t this_thread = pthread_self();

    if (rc != 0) {
        if(rc==EINVAL)
            printf("EINVAL");
        if(rc==ENOTSUP)
            printf("ENOTSUP");
        exit(10);
    } else 
    s = pthread_getattr_np(pthread_self(), &attr);
    
    if (s != 0) {
        printf("pthread_getattr_np");
        exit(11);
    }
    
    display_pthread_attr(&attr, arg_wsk);

    czasozajmowacz();

    pthread_exit((void*)RETVAL);
}


int main() {
    pthread_t tid[WATKI];
    int *retval[WATKI];
    pthread_attr_t attr;
    size_t stacksize;
    srand(time(NULL));
    int i, s, ret, rc;
    int err;
    struct sched_param param;

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    for (i = 0; i < WATKI; i++) CPU_SET(i, &cpuset);

    for (i = 0; i < WATKI; i++) {

        if (pthread_attr_init(&attr) == -1) {
            printf("Error: pthread_attr_init");
            exit(1);
        }

        //s = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
        //s = pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);
        s = pthread_attr_setinheritsched(&attr, rand()%2);
        if (s != 0)
            handle_error_en(s, "pthread_attr_setinheritsched");

        //s = pthread_attr_setschedpolicy(&attr, SCHED_OTHER);
        //s = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
        //s = pthread_attr_setschedpolicy(&attr, SCHED_RR);
        s = pthread_attr_setschedpolicy(&attr, rand()%3);
        if (s != 0) {

            printf("Error: pthread_attr_setschedpolicy returned\n");
            exit(2);
        }

        //PRIORITY
        param.sched_priority = rand()%20 + 1;
        if (pthread_attr_setschedparam(&attr, &param) != 0) {
            printf("Error: pthread_attr_setschedparam");
        }

        //DETACH, JOINABLE
        if (pthread_attr_setdetachstate(&attr, rand()%2)) {
            printf("Error: pthread_attr_setdetachstate");
            exit(3);
        }

        stacksize = PTHREAD_STACK_MIN * (rand() % 20 + 1);
        if (pthread_attr_setstacksize(&attr, stacksize) == -1) {
            printf("Error: pthread_attr_setstacksize");
            exit(4);
        }


        printf("\twatek glowny: tworzenie watku potomnego nr %d\n", i + 1);
        if (pthread_create(&tid[i], &attr, zadanie_watku, (void *) i + 1) == -1) {
            printf("Error: pthread_create");
            exit(5);
        }

        sleep(2);
        if (pthread_attr_destroy(&attr) != 0) {
            printf("Error: pthread_attr_destroy");
            exit(6);
        }

        printf("\n");
    }

    for (i = 0; i < WATKI; ++i) {

        if (pthread_join(tid[i], (void *) &retval[i]) == -1) {
            printf("Error: pthread_join");
            exit(7);
        }
        if(retval[i] == RETVAL) {
            printf("Watek: %d - Kod powrotu: %d (JOINABLE)\n", i+1, retval[i]);
        } else {
            printf("Watek: %d - Kod powrotu: %d (DETACHED)\n", i+1, retval[i]);
        }
        
    }

    pthread_exit(NULL);
}

