#define _GNU_SOURCE  
#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<limits.h>
#include<errno.h>
#include<sched.h>
#include<sys/syscall.h>

#define WYMIAR 1000
#define ROZMIAR WYMIAR*WYMIAR
#define WATKI 4

int zmienna_wspolna=0;
double a[ROZMIAR],b[ROZMIAR],c[ROZMIAR];


double czasozajmowacz(){
  int i, j, k;
  int n=WYMIAR;
  for(i=0;i<ROZMIAR;i++) a[i]=1.0*i;
  for(i=0;i<ROZMIAR;i++) b[i]=1.0*(ROZMIAR-i);
  for(i=0;i<n;i++){
    for(j=0;j<n;j++){
      c[i+n*j]=0.0;
      for(k=0;k<n;k++){
	c[i+n*j] += a[i+n*k]*b[k+n*j];
      }
    }
  }
  return(c[ROZMIAR-1]);
}

#define test_errno(msg) do{if (errno) {perror(msg); exit(EXIT_FAILURE);}} while(0)
#define handle_error_en(en, msg) \
               do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)
#define errExitEN(en, msg) \
                        do { errno = en; perror(msg); \
                             exit(EXIT_FAILURE); } while (0)

static void
display_pthread_attr(pthread_attr_t *attr)
{
    int s;
    size_t stacksize;
    size_t guardsize;
    int policy;
    struct sched_param schedparam;
    int detachstate;
    int inheritsched;
	int stackaddr;

	errno = pthread_attr_getstack(attr, &stackaddr, &stacksize);
	test_errno("pthread_attr_getstackaddr");
	printf("Stack addr:          %x\n", stackaddr);

    //s = pthread_attr_getstacksize(attr, &stacksize);
    // if (s != 0)
    //     errExitEN(s, "pthread_attr_getstacksize");
    printf("Stack size:          %zd\n", stacksize);

    s = pthread_attr_getguardsize(attr, &guardsize);
    if (s != 0)
        errExitEN(s, "pthread_attr_getguardsize");
    printf("Guard size:          %zd\n", guardsize);

    s = pthread_attr_getschedpolicy(attr, &policy);
    if (s != 0)
        errExitEN(s, "pthread_attr_getschedpolicy");
    printf("Scheduling policy:   %s\n",
            (policy == SCHED_FIFO) ? "SCHED_FIFO" :
            (policy == SCHED_RR) ? "SCHED_RR" :
            (policy == SCHED_OTHER) ? "SCHED_OTHER" : "[unknown]");

    s = pthread_attr_getschedparam(attr, &schedparam);
    if (s != 0)
        errExitEN(s, "pthread_attr_getschedparam");
    printf("Scheduling priority: %d\n", schedparam.sched_priority);

    s = pthread_attr_getdetachstate(attr, &detachstate);
    if (s != 0)
        errExitEN(s, "pthread_attr_getdetachstate");
    printf("Detach state:        %s\n",
            (detachstate == PTHREAD_CREATE_DETACHED) ? "DETACHED" :
            (detachstate == PTHREAD_CREATE_JOINABLE) ? "JOINABLE" :
            "???");

    s = pthread_attr_getinheritsched(attr, &inheritsched);
    if (s != 0)
        errExitEN(s, "pthread_attr_getinheritsched");
    printf("Inherit scheduler:   %s\n",
            (inheritsched == PTHREAD_INHERIT_SCHED) ? "INHERIT" :
            (inheritsched == PTHREAD_EXPLICIT_SCHED) ? "EXPLICIT" :
            "???");


}



void * zadanie_watku (void * arg_wsk)
{
  // int policy;
  // struct sched_param param;
	// pthread_getschedparam(pthread_self(), &policy, &param);
  // param.sched_priority = sched_get_priority_max(policy);
  // pthread_setschedparam(pthread_self(), policy, &param);

  // struct sched_param params;
  // params.sched_priority = sched_get_priority_max(SCHED_FIFO);
  // pthread_setschedparam(pthread_self(), SCHED_FIFO, &params);

	int nr = (int)arg_wsk;
	//pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	//printf("\twatek potomny [nr = %d]: uniemozliwione zabicie\n", nr);

  int i;
  for(i = 0; i < 3; i++)
	  czasozajmowacz();


	pthread_attr_t attr;
	pthread_getattr_np(pthread_self(), &attr);
	//syscall(SYS_gettid);
	printf("\n");
	//wyswietl_atrybuty(&attr, (int)arg_wsk);
	display_pthread_attr(&attr);

    cpu_set_t cpuset;
    pthread_t thread;
    thread = pthread_self();
    int s, j;

    s = pthread_getaffinity_np(thread, sizeof(cpu_set_t), &cpuset);
    if (s != 0)
        handle_error_en(s, "pthread_getaffinity_np");

    //Wyświetlanie przydziału CPU
    // printf("Set returned by pthread_getaffinity_np() contained:\n");
    // for (j = 0; j < CPU_SETSIZE; j++)
    //     if (CPU_ISSET(j, &cpuset))
    //         printf("    CPU %d\n", j);

	//printf("\twatek potomny [nr = %d]: umozliwienie zabicia\n", nr);
	//pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

	//pthread_testcancel();

	//zmienna_wspolna++;
	//printf("\twatek potomny [nr = %d]: zmiana wartosci zmiennej wspolnej\n", nr);

	return(NULL);
}

int main()
{
  pthread_t tid[WATKI];
  int retval[WATKI];
	int i, error;

  struct sched_param param;
  pthread_attr_t attr;
  size_t stacksize[WATKI];
  int policy[WATKI];
  int priority[WATKI];
  

  stacksize[0] = PTHREAD_STACK_MIN*5;
  policy[0] = SCHED_OTHER;
  priority[0] = 60;

  stacksize[1] = PTHREAD_STACK_MIN;
  policy[1] = SCHED_FIFO;
  priority[1] = 10;

  stacksize[2] = PTHREAD_STACK_MIN*2;
  policy[2] = SCHED_RR;
  priority[2] = 80;

  stacksize[3] = PTHREAD_STACK_MIN*4;
  policy[3] = SCHED_RR;
  priority[3] = 35;

  int s, ret;
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  for (i = 0; i < WATKI; i++) CPU_SET(i, &cpuset);

  for(i = 0; i < WATKI; i++) {
	  //pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    if (pthread_attr_init(&attr) == -1) {
          printf("Error: pthread_attr_init");
          exit(1);
    }

 	s = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
	if (s != 0)
	 	handle_error_en(s, "pthread_attr_setinheritsched");
	
if (error = pthread_attr_setschedpolicy(&attr, SCHED_OTHER) != 0) {
        printf("Error: pthread_attr_setschedpolicy returned %d", error);
        exit(3);
    }


	//ret = pthread_attr_getschedparam (&attr, &param);
  
	param.sched_priority = priority[i];
  printf("Priority %d\n", param.sched_priority);
	ret = pthread_attr_setschedparam (&attr, &param);

    //Stacksize
    if (pthread_attr_setstacksize(&attr, stacksize[i]) == -1) {
          printf("Error: pthread_attr_setstacksize");
          exit(2);
    }


    //Scheduling parameters
    s = pthread_attr_setinheritsched(&attr, rand()%1);
	  if (s != 0)
	 	handle_error_en(s, "pthread_attr_setinheritsched");


    //param.sched_priority = sched_get_priority_max(policy[i]);
    // if (pthread_attr_setschedparam(&attr, &param) != 0) {
    //     printf("Error: pthread_attr_setschedparam");
    // }
	//printf("Priority: %d", param.sched_priority);

    //CPU affinity of a thread
    if (pthread_setaffinity_np(&tid[i], sizeof(cpu_set_t), &cpuset) == -1) {
        printf("Error: pthread_setaffinity_np");
        exit(4);
    }

    printf("\twatek glowny: tworzenie watku potomnego nr %d\n", i);
    if(ret = pthread_create(&tid[i], &attr, zadanie_watku, (void *) i) == -1) {
      printf("Error: pthread_create");
      exit(5);
    }
    //printf("ret: %d\n", ret);
	 //display_pthread_attr(&attr);
	 //pthread_attr_destroy(&attr);
  }
  
  for(i = 0; i < WATKI; i++) {
    if(pthread_join(tid[i], (void*) &retval[i]) == -1) {
      printf("Error: pthread_join");
      exit(6);
    } else {
    printf("\nZakonczono watek nr %d. Kod powrotu: %d\n", i, retval[i]);
  }
  }


	pthread_exit(NULL);
}


