#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <stddef.h>
#include <string.h>

typedef struct {
        int id;
        char codeName[30];
        char cpuNumber[20];
        double baseFrequency;
        int busSpeed;
        int lithography;
        double maxMemoryBandiwdth;
} CpuData;

int main(int argc, char **argv) {
    const int tag = 13;
    int i, size, rank;
	double start, end;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size < 2) {
        fprintf(stderr,"Requires at least two processes.\n");
        exit(-1);
    }

	//Create new MPI data type
    const int count = 7;
    int blocklengths[7] = { 1, 30, 20, 1, 1, 1, 1 };
    MPI_Datatype types[7] = {MPI_INT, MPI_CHAR, MPI_CHAR, MPI_DOUBLE, MPI_INT, MPI_INT, MPI_DOUBLE};
    MPI_Datatype mpi_cpudata_type;
    MPI_Aint offsets[7];

    offsets[0] = offsetof(CpuData, id);
    offsets[1] = offsetof(CpuData, codeName);
    offsets[2] = offsetof(CpuData, cpuNumber);
    offsets[3] = offsetof(CpuData, baseFrequency);
    offsets[4] = offsetof(CpuData, busSpeed);
    offsets[5] = offsetof(CpuData, lithography);
    offsets[6] = offsetof(CpuData, maxMemoryBandiwdth);

    MPI_Type_create_struct(count, blocklengths, offsets, types, &mpi_cpudata_type);
    MPI_Type_commit(&mpi_cpudata_type);

    //Set the data
    CpuData cpuData;
    cpuData.id = 1;
    strcpy(cpuData.codeName, "CPU example name");
    strcpy(cpuData.cpuNumber, "CPU example number");
    cpuData.baseFrequency = 2.5;
    cpuData.busSpeed = 4;
    cpuData.lithography = 22;
    cpuData.maxMemoryBandiwdth = 10.5;

	start = MPI_Wtime();

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0) {
        const int dest = 1;

		MPI_Send(&cpuData, 1, mpi_cpudata_type, dest, tag, MPI_COMM_WORLD);
    } else if (rank == 1) {
        MPI_Status status;
        const int src = 0;
        CpuData rcvCpuData;

        MPI_Recv(&rcvCpuData, 1, mpi_cpudata_type, src, tag, MPI_COMM_WORLD, &status);
        printf("Rank: %d, Received data structure: \n", rank);
        printf("Id: %d\n", rcvCpuData.id);
        printf("Code name: %s\n", rcvCpuData.codeName);
        printf("CPU number: %s\n", rcvCpuData.cpuNumber);
        printf("Base frequency: %lf\n", rcvCpuData.baseFrequency);
        printf("Bus speed: %d\n", rcvCpuData.busSpeed);
        printf("Lithography: %d\n", rcvCpuData.lithography);
        printf("Max memory bandwidth: %lf\n", rcvCpuData.maxMemoryBandiwdth);

    }

	end = MPI_Wtime();
    MPI_Type_free(&mpi_cpudata_type);
    MPI_Finalize();
	
	if(rank == 0) {
        double time = end - start;
		printf("\nTime: %lf\n", end - start);
        char filename[20] = "zad3.txt";
        FILE *fp = fopen(filename, "a");
        fprintf(fp, "%lf\n", time);
	}


    return 0;
}