#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <stddef.h>
#include <string.h>

typedef struct {
        int id;
        char codeName[30];
        char cpuNumber[20];
        double baseFrequency;
        int busSpeed;
        int lithography;
        double maxMemoryBandiwdth;
} CpuData;

int main(int argc, char **argv) {
	int numberOfStructures = 10000000;
    const int tag = 13;
    int i, size, rank, position;
	double start, end;
    char buff[78];

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size < 2) {
        fprintf(stderr,"Requires at least two processes.\n");
        exit(-1);
    }

	//Create new MPI data type
    const int count = 7;
    int blocklengths[7] = { 1, 30, 20, 1, 1, 1, 1 };
    MPI_Datatype types[7] = {MPI_INT, MPI_CHAR, MPI_CHAR, MPI_DOUBLE, MPI_INT, MPI_INT, MPI_DOUBLE};
    MPI_Datatype mpi_cpudata_type;
    MPI_Aint offsets[7];

    offsets[0] = offsetof(CpuData, id);
    offsets[1] = offsetof(CpuData, codeName);
    offsets[2] = offsetof(CpuData, cpuNumber);
    offsets[3] = offsetof(CpuData, baseFrequency);
    offsets[4] = offsetof(CpuData, busSpeed);
    offsets[5] = offsetof(CpuData, lithography);
    offsets[6] = offsetof(CpuData, maxMemoryBandiwdth);

    MPI_Type_create_struct(count, blocklengths, offsets, types, &mpi_cpudata_type);
    MPI_Type_commit(&mpi_cpudata_type);

    //Set the data
    // CpuData* cpuData = (CpuData*)malloc(numberOfStructures * sizeof(CpuData));
    // for(i = 0; i < numberOfStructures; i++) {
    //     cpuData[i].id = i + 1;
    //     strcpy(cpuData[i].codeName, "CPU example name");
    //     strcpy(cpuData[i].cpuNumber, "CPU example number");
    //     cpuData[i].baseFrequency = 2.5;
    //     cpuData[i].busSpeed = 4;
    //     cpuData[i].lithography = 22;
    //     cpuData[i].maxMemoryBandiwdth = 10.5;
    // }
    
    CpuData cpuData;
    cpuData.id = 1;
    strcpy(cpuData.codeName, "CPU example name");
    strcpy(cpuData.cpuNumber, "CPU example number");
    cpuData.baseFrequency = 2.5;
    cpuData.busSpeed = 4;
    cpuData.lithography = 22;
    cpuData.maxMemoryBandiwdth = 10.5;

	start = MPI_Wtime();

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0) {
        const int dest = 1;

        int buffer_attached_size = MPI_BSEND_OVERHEAD + sizeof(CpuData);
        char* buffer_attached = (char*)malloc(buffer_attached_size);
        
        for(i = 0; i < numberOfStructures; i++) {
            position = 0;
            MPI_Pack(&cpuData, 1, mpi_cpudata_type, buff, sizeof(buff), &position, MPI_COMM_WORLD);

            MPI_Buffer_attach(buffer_attached, buffer_attached_size);
            MPI_Bsend(buff, sizeof(buff), MPI_PACKED, dest, tag, MPI_COMM_WORLD);
            MPI_Buffer_detach(&buffer_attached, &buffer_attached_size);
        }
        free(buffer_attached);

    } else if (rank == 1) {
        MPI_Status status;
        const int src = 0;
        CpuData rcvCpuData;

        for(i = 0; i < numberOfStructures; i++) {
            position = 0;
            MPI_Recv(&buff, sizeof(buff), MPI_PACKED, src, tag, MPI_COMM_WORLD, &status);
            MPI_Unpack(buff, sizeof(buff), &position, &rcvCpuData, 1, mpi_cpudata_type, MPI_COMM_WORLD);
            // if(i == 1000) {
            //     printf("Rank: %d, Received data structure: \n", rank);
            //     printf("Id: %d\n", rcvCpuData.id);
            //     printf("Code name: %s\n", rcvCpuData.codeName);
            //     printf("CPU number: %s\n", rcvCpuData.cpuNumber);
            //     printf("Base frequency: %lf\n", rcvCpuData.baseFrequency);
            //     printf("Bus speed: %d\n", rcvCpuData.busSpeed);
            //     printf("Lithography: %d\n", rcvCpuData.lithography);
            //     printf("Max memory bandwidth: %lf\n", rcvCpuData.maxMemoryBandiwdth);
            // }
        }
    }

	end = MPI_Wtime();
    MPI_Type_free(&mpi_cpudata_type);
    MPI_Finalize();
	
	if(rank == 0) {
        double time = end - start;
		printf("\nTime: %lf\n", end - start);
        char filename[20] = "zad6-pack-Bsend.txt";
        FILE *fp = fopen(filename, "a");
        fprintf(fp, "%lf\n", time);
	}

    return 0;
}