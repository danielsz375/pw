#include <stdio.h>
#include <omp.h>


int main() {
	int i, a;
	a = 7;
    double start, end;
    int suma = 0;
    omp_lock_t lock;
    omp_init_lock(&lock);

    start = omp_get_wtime();
    #pragma omp parallel for shared(suma) num_threads(4) schedule(static, 3) 
    for(i=0;i<500;i++)
    {
        omp_set_lock(&lock);
        suma += a*a;
        omp_unset_lock(&lock);
        
    }
    end = omp_get_wtime();
    omp_destroy_lock(&lock);
    printf("Czas: %f\n",end - start);
    printf("Suma: %d\n",suma);

	return 0;
}
