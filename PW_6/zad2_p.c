#include <stdio.h>
#include <omp.h>


int main() {
	int i, a;
	a = 7;

        printf("#pragma omp parallel for private(a) num_threads(7)\n\n");

        #pragma omp parallel for private(a) num_threads(7) 

        for(i=0;i<10;i++)
            {
                printf("Thread %d a=%d\n",omp_get_thread_num(),a);
                a++;
            }
        printf("a=%d\n", a);


	return 0;
}
