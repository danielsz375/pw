#include <stdio.h>
#include <omp.h>


int main() {
	int i, a;
	a = 7;
    double start, end;
    int suma = 0;

    start = omp_get_wtime();
    #pragma omp parallel for num_threads(4) reduction(+ : suma) schedule(static, 3) 
    for(i=0;i<500;i++)
    {
        suma += a*a;
        
    }
    end = omp_get_wtime();
    printf("Czas: %f\n",end - start);
    printf("Suma: %d\n",suma);

	return 0;
}
