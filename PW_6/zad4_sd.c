#include <stdio.h>
#include <omp.h>


int main() {
	int i, a;
	a = 7;
    double start, end;

    printf("schedule(static)\n\n");
    start = omp_get_wtime();
    #pragma omp parallel for firstprivate(a) num_threads(4) schedule(static) 

    for(i=0;i<10000000;i++)
        {
            a++;
        }
    end = omp_get_wtime();
    printf("Static, czas: %f\n",end - start);


	return 0;
}
