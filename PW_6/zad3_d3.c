#include <stdio.h>
#include <omp.h>


int main() {
	int i, a;
	a = 7;
    
    printf("schedule(dynamic, 3)\n\n");
    #pragma omp parallel for firstprivate(a) num_threads(4) schedule(dynamic, 3) 

    for(i=0;i<15;i++)
        {
            printf("Thread %d a=%d\n",omp_get_thread_num(),a);
            a++;
        }



	return 0;
}
