#include <stdio.h>
#include <omp.h>


int main() {
	int i, a;
	a = 7;
    
    printf("schedule(static)\n\n");
    #pragma omp parallel for firstprivate(a) num_threads(4) schedule(static) 

    for(i=0;i<15;i++)
        {
            printf("Thread %d a=%d\n",omp_get_thread_num(),a);
            a++;
        }



	return 0;
}
