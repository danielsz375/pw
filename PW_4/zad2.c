#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>

#define KLIENCI 5
#define KUFLE 3
#define MAX_CZAS 7

pthread_mutex_t kufel[KUFLE];

void* zadanie_watku(void* klient_id) {
    int i, j, czas;

    
    for(i = 0; i < 2; i++) {
        for(j = 0; j < KUFLE; j++) {
            if(pthread_mutex_trylock(&kufel[j]) == 0) {
                printf("\tKlient nr %d zamawia piwo w kuflu nr %d\n", klient_id, j + 1);
                czas = rand()%MAX_CZAS + 1;
                sleep(czas);
                printf("\tKlient nr %d oddaje kufel nr %d po czasie %d s\n", klient_id, j + 1, czas);
                pthread_mutex_unlock(&kufel[j]);
                break;
            } 
            if(j == KUFLE - 1) j = 0;
                continue;
        }
    }

    printf("Klient nr %d wychodzi z pubu.\n", klient_id);
    pthread_exit(0);
}

int main()
{
    pthread_attr_t attr;
    pthread_t klient[KLIENCI];
    int* retval[KLIENCI];
    int i;

    srand(time(NULL));
    
    pthread_attr_init(&attr);
    if(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0) {
        printf("Error: pthread_attr_setdetachstate");
        exit(1);
    }

    for(i = 0; i < KUFLE; i++)
        pthread_mutex_init(&kufel[i], NULL);

    for(i = 0; i < KLIENCI; i++) {
        printf("Klient nr %d wchodzi do pubu.\n", i + 1);
        if(pthread_create(&klient[i], &attr, zadanie_watku, i + 1) == -1 ) {
            printf("Error: pthread_create");
            exit(2);
        }
    }

    // for(i = 0; i < KLIENCI; i++) {
    //     if(pthread_join(klient[i], (void*)&retval[i]) == -1 ) {
    //         printf("Error: pthread_join");
    //         exit(3);
    //     }
    //     //printf("\tKlient nr %d wyszedł z pubu. Kod powrotu: %d\n", i + 1, (int)retval[i]);
    //     printf("\tKlient nr %d wyszedł z pubu.\n", i + 1);
    // }
    
	pthread_exit(NULL); 
}


