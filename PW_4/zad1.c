#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>

int zmienna_wspolna=0;
pthread_mutex_t mutex;

#define WYMIAR 1000
#define ROZMIAR WYMIAR*WYMIAR
#define WATKI 100000
double a[ROZMIAR],b[ROZMIAR],c[ROZMIAR];


void* inkrementacja(void* arg_wsk) {
    int i;
    for(i = 0; i < WATKI; i++) {
        pthread_mutex_lock(&mutex);
        zmienna_wspolna++;
        pthread_mutex_unlock(&mutex);
    }
        
    pthread_exit(NULL);
}

void* dekrementacja(void* arg_wsk) {
    int i;
    for(i = 0; i < WATKI; i++) {
        pthread_mutex_lock(&mutex);
        zmienna_wspolna--;
        pthread_mutex_unlock(&mutex);
    }

    pthread_exit(NULL);
}



int main()
{
    void (*funkcja_watku[2])(void*) = {inkrementacja, dekrementacja};
    pthread_t tid[2];
    int* retval[2];
    int i;

    pthread_mutex_init(&mutex, NULL);

    for(i = 0; i < 2; i++) {
        printf("\twatek glowny: tworzenie watku potomnego nr %d\n", i+1);
        if(pthread_create(&tid[i], NULL, (*funkcja_watku[i]), NULL) == -1 ) {
            printf("Error: pthread_create");
            exit(1);
        }
    }

    for(i = 0; i < 2; i++) {
        if(pthread_join(tid[i], (void*)&retval[i]) == -1 ) {
            printf("Error: pthread_join");
            exit(2);
        }
        printf("\twatek glowny: kod powrotu watku potomnego nr %d: %d\n", i+1, (int)retval[i]);
    }
    
    printf("\tzmienna_wspolna = %d\n", zmienna_wspolna);


	pthread_exit(NULL); 
}


