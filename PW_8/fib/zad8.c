#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include<time.h>

int fibo(int n) {
    if(n < 2) {
        return n;
    }

    return fibo(n - 2) + fibo(n - 1);
}

int fibo_omp(int n) {
    int i, j;
    if(n < 2) {
        return n;
    }

    #pragma omp task shared(i) firstprivate(n)
    i = fibo_omp(n - 2);

    #pragma omp task shared(j) firstprivate(n)
    j = fibo_omp(n - 1);

    #pragma omp taskwait
    return i + j;
}

int fibo_omp_limited(int n) {
    int i, j;
    if(n < 2) {
        return n;
    }   

    if(n > 20) {
        return fibo(n);
    }  

    #pragma omp task shared(i) firstprivate(n)
    i = fibo_omp_limited(n - 2);

    #pragma omp task shared(j) firstprivate(n)
    j = fibo_omp_limited(n - 1);

    #pragma omp taskwait
    return i + j;
}

int fibo_omp2(int n) {
    int i, j;
    if(n < 2) {
        return n;
    }

    #pragma omp task shared(i)
    i = fibo(n - 2);

    #pragma omp task shared(j)
    j = fibo(n - 1);

    #pragma omp taskwait
    return i + j;
}


int main() {
    srand(time(NULL));
    int i, el, n = 40;
    double start, end;
    double time[4];

    FILE* fptr;
    char filename1[12] = "zad8-1.txt";
    char filename2[12] = "zad8-2.txt";
    char filename3[12] = "zad8-3.txt";
    char filename4[12] = "zad8-4.txt";

    start = omp_get_wtime();
    el = fibo(n);
    end = omp_get_wtime();
    time[0] = end - start;
    printf("Element nr %d ciągu Fibonacciego: %d\n", n, el);

    start = omp_get_wtime();
    el = fibo_omp(n);
    end = omp_get_wtime();
    time[1] = end - start;
    printf("Element nr %d ciągu Fibonacciego: %d\n", n, el);

    start = omp_get_wtime();
    el = fibo_omp_limited(n);
    end = omp_get_wtime();
    time[2] = end - start;
    printf("Element nr %d ciągu Fibonacciego: %d\n", n, el);

    start = omp_get_wtime();
    el = fibo_omp2(n);
    end = omp_get_wtime();
    time[3] = end - start;
    printf("Element nr %d ciągu Fibonacciego: %d\n", n, el);

    printf("Czasy obliczeń:\n");
    printf("Rekurencyjnie: %lf\n", time[0]);
    printf("Używając dyrektywy task (bez ograniczenia): %lf\n", time[1]);
    printf("Używając dyrektywy task (ograniczenie if(n > 20)): %lf\n", time[2]);
    printf("Używając dyrektywy task (ograniczenie przez wykorzystanie wyłącznie 2 tasków): %lf\n", time[3]);

    fptr = fopen(filename1, "a");
    if(!fptr) {
        printf("Błąd otwarcia pliku!");
    } else {
        fprintf(fptr, "%lf\n", time[0]);
    }

    fptr = fopen(filename2, "a");
    if(!fptr) {
        printf("Błąd otwarcia pliku!");
    } else {
        fprintf(fptr, "%lf\n", time[1]);
    }

    fptr = fopen(filename3, "a");
    if(!fptr) {
        printf("Błąd otwarcia pliku!");
    } else {
        fprintf(fptr, "%lf\n", time[2]);
    }

    fptr = fopen(filename4, "a");
    if(!fptr) {
        printf("Błąd otwarcia pliku!");
    } else {
        fprintf(fptr, "%lf\n", time[3]);
    }

    return 0;
}