#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include<time.h>

int main() {
    srand(time(NULL));
    int i;
    double start, end;

    FILE* fptr;
    char filename1[12] = "zad3-1.txt";
    char filename2[12] = "zad3-2.txt";
    char filename3[12] = "zad3-3.txt";

    //int length = 1000000000;
    int length = 100000000;
    double* vec = (double*)malloc(length * sizeof(double));
    double* vec2 = (double*)malloc(length * sizeof(double));
    double res = 0;

    double time[3];

    if(vec == NULL || vec2 == NULL) {
        printf("Błąd alokacji pamięci!");
        exit(1);
    }

    for(i = 0; i < length; i++) {
        vec[i] = (double)rand()/RAND_MAX*100.0;
        vec2[i] = (double)rand()/RAND_MAX*100.0;
    }

    // printf("Wektor 1: ");
    // for(i = 0; i < 5; i++) {
    //     printf("%lf ", vec[i]);
    // }

    // printf("\nWektor 2: ");
    // for(i = 0; i < 5; i++) {
    //     printf("%lf ", vec2[i]);
    // }

    start = omp_get_wtime();
    for(i = 0; i < length; i++) {
        res += vec[i] * vec2[i];
    }
    end = omp_get_wtime();
    time[0] = end - start;
    
    printf("Wynik: %lf\n", res);
    res = 0;

    start = omp_get_wtime();
    #pragma omp parallel for reduction(+:res) private(i) num_threads(8)
    for(i = 0; i < length; i++) {
        res += vec[i] * vec2[i];
    }
    end = omp_get_wtime();
    time[1] = end - start;

    printf("Wynik: %lf\n", res);
    res = 0;




    start = omp_get_wtime();
    #pragma omp simd
    for(i = 0; i < length; i++) {
        res += vec[i] * vec2[i];
    }
    end = omp_get_wtime();
    time[2] = end - start;

    printf("Wynik: %lf\n", res);
    res = 0;

    printf("Czasy obliczeń:\n");
    printf("Bez zrównoleglenia: %lf\n", time[0]);
    printf("Zrównoleglenie omp parallel for (8 wątków): %lf\n", time[1]);
    printf("Zrównoleglenie omp simd: %lf\n", time[2]);

    fptr = fopen(filename1, "a");
    if(!fptr) {
        printf("Błąd otwarcia pliku!");
    } else {
        fprintf(fptr, "%lf\n", time[0]);
    }

    fptr = fopen(filename2, "a");
    if(!fptr) {
        printf("Błąd otwarcia pliku!");
    } else {
        fprintf(fptr, "%lf\n", time[1]);
    }

    fptr = fopen(filename3, "a");
    if(!fptr) {
        printf("Błąd otwarcia pliku!");
    } else {
        fprintf(fptr, "%lf\n", time[2]);
    }


    free(vec);
    free(vec2);

    return 0;
}